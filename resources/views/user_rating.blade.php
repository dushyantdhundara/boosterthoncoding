@extends('layouts.header')

@section('content')



<!-- PAGE CONTAINER-->
<div class="page-container">
    <!-- HEADER DESKTOP-->
    <header class="header-desktop">
        <div class="section__content section__content--p30">
            <div class="container-fluid">
                <div class="header-wrap">
                    <form class="form-header" action="" method="POST">

                    </form>
                    <div class="header-button">

                        <div class="account-wrap">
                            <div class="account-item clearfix js-item-menu">

                                <div class="account-dropdown__footer">
                                    <a href="{{route('logout')}}">
                                        <i class="zmdi zmdi-power"></i>Logout</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </header>


    <div class="main-content">
        <div class="section__content section__content--p30">
            <div class="container-fluid">

                <div class="row m-t-30">
                    <div class="col-md-12">
                        <!-- DATA TABLE-->
                        <div class="table-responsive m-b-40">
                            <table class="table table-borderless table-data3">
                                <thead>
                                    <tr>
                                        <th>S.No</th>
                                        <th>Fundraiser Name</th>
                                        <th>Reviewer  Name</th>
                                        <th>Reviewer Email</th>
                                        <th>Rating</th>
                                        <th>Review</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    
                                    <?php  $number = 1; ?>
                                      @foreach($result as $row)
						
					<tr>
						<td><?php echo  $number++; ?></td>
						<td>{{$row->fundraiser_name}}</td>
						<td>{{$row->name}}</td>
						<td>{{$row->email}}</td>
						<td>{{$row->rating}}</td>
						<td>{{$row->review}}</td>
					</tr>
					@endforeach

                                </tbody>
                            </table>
                        </div>
                        <!-- END DATA TABLE-->
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>

<script src="{{asset('public/vendor/jquery-3.2.1.min.js')}}"></script>

<script src="{{asset('public/vendor/bootstrap-4.1/popper.min.js')}}"></script>
<script src="{{asset('public/vendor/bootstrap-4.1/bootstrap.min.js')}}"></script>

<script src="{{asset('public/vendor/slick/slick.min.js')}}">
</script>
<script src="{{asset('public/vendor/wow/wow.min.js')}}"></script>
<script src="{{asset('public/vendor/animsition/animsition.min.js')}}"></script>
<script src="{{asset('public/vendor/bootstrap-progressbar/bootstrap-progressbar.min.js')}}">
</script>
<script src="{{asset('public/vendor/counter-up/jquery.waypoints.min.js')}}"></script>
<script src="{{asset('public/vendor/counter-up/jquery.counterup.min.js')}}">
</script>
<script src="{{asset('public/vendor/circle-progress/circle-progress.min.js')}}"></script>
<script src="{{asset('public/vendor/perfect-scrollbar/perfect-scrollbar.js')}}"></script>
<script src="{{asset('public/vendor/chartjs/Chart.bundle.min.js')}}"></script>
<script src="{{asset('public/vendor/select2/select2.min.js')}}">
</script>
<!--<script src="{{asset('public/js/main.js')}}"></script>-->

</body>

</html>
<!-- end document-->
@endsection