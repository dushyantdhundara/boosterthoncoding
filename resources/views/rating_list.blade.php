<!DOCTYPE html>
<html>
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.1.0/css/all.css" integrity="sha384-lKuwvrZot6UHsBSfcMvOkWwlCMgc0TaWr+30HWe3a4ltaBwTZhyTEggF5tJv8tbt" crossorigin="anonymous">
<link href="//netdna.bootstrapcdn.com/bootstrap/3.0.1/css/bootstrap.min.css" rel="stylesheet" >


<script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
<script src="//netdna.bootstrapcdn.com/bootstrap/3.0.1/js/bootstrap.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/sweetalert/2.1.2/sweetalert.min.js"></script>


<link href="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet">




<style>
    /*     .animated {
        -webkit-transition: height 0.2s;
        -moz-transition: height 0.2s;
        transition: height 0.2s;
    }*/
    .stars
    {
        margin: 20px 0;
        font-size: 24px;
        color: #d17581;
    }
    #country-list {
        float: left;
        list-style: none;
        margin-top: -3px;
        padding: 0;
        width: 190px;
        position: absolute;
        left: 25px;
        top: 33px;
        z-index: 999;
    }

    #country-list li {
        padding: 10px;
        background: #f0f0f0;
        border-bottom: #bbb9b9 1px solid;
    }

    .rv_bx{ 

        border-radius:5px; 
        background:#fff !important; 
        padding:30px; border:none;    
        box-shadow: -1px 4px 9px #92a9c173;
        margin: 0 auto;
        max-width: 547px;
    }
    .rv_bx .input-group-text{ background:#0062cc;
                              border-color:#0062cc;
    }
    .rv_bx .text-info{color:#fff !important;

    }
    .rv_bx .form-control{
        box-shadow: none;
        border: 2px solid #d6d6d6;
    }
    .rv_bx .form-control:focus{
        box-shadow: none;
        border: 2px solid #0062cc;
    }
    .table-responsive {
    font-size: 16px;
    }
    .btn-primary {
    font-size: 12px;
    }
</style>

<body style="background: #ececec;">


<div class="container">
    <div class="row" style="margin-top:40px;">
        <div class="col-md-12">
            <div class="well well-sm rv_bx">


                <div class="row" id="post-review-box">
                    <div class="col-md-12">
                        <div class="table-responsive">          
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th>S.No</th>
                                        <th>Fundraiser name</th>
                                        <th>Rating</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                  <tbody>
                                    <?php $number = 1; ?>
                                       @foreach($result as $row)
						
					<tr>
						<td><?php   echo  $number++; ?></td>
						<td>{{$row['name']}}</td>
						<td>{{$row['rating']}}</td>
                                                <td><a class="btn btn-primary" href="{{route('reportpanding')}}/{{$row['id']}}"> Click to rate </a></td>
					</tr>
					@endforeach
                                </tbody>
                            </table>
                        </div>
                          <div class="text-right"><a href="{{route('/')}}" class="btn btn-primary">Didn't find your fundraiser</a></div>
                    </div>
                  

                </div>
            </div>
        </div>

    </div>
</div>
    
</body>
</html>

<script>
    (function (e) {
        var t, o = {className: "autosizejs", append: "", callback: !1, resizeDelay: 10}, i = '<textarea tabindex="-1" style="position:absolute; top:-999px; left:0; right:auto; bottom:auto; border:0; padding: 0; -moz-box-sizing:content-box; -webkit-box-sizing:content-box; box-sizing:content-box; word-wrap:break-word; height:0 !important; min-height:0 !important; overflow:hidden; transition:none; -webkit-transition:none; -moz-transition:none;"/>', n = ["fontFamily", "fontSize", "fontWeight", "fontStyle", "letterSpacing", "textTransform", "wordSpacing", "textIndent"], s = e(i).data("autosize", !0)[0];
        s.style.lineHeight = "99px", "99px" === e(s).css("lineHeight") && n.push("lineHeight"), s.style.lineHeight = "", e.fn.autosize = function (i) {
            return this.length ? (i = e.extend({}, o, i || {}), s.parentNode !== document.body && e(document.body).append(s), this.each(function () {
                function o() {
                    var t, o;
                    "getComputedStyle"in window ? (t = window.getComputedStyle(u, null), o = u.getBoundingClientRect().width, e.each(["paddingLeft", "paddingRight", "borderLeftWidth", "borderRightWidth"], function (e, i) {
                        o -= parseInt(t[i], 10)
                    }), s.style.width = o + "px") : s.style.width = Math.max(p.width(), 0) + "px"
                }
                function a() {
                    var a = {};
                    if (t = u, s.className = i.className, d = parseInt(p.css("maxHeight"), 10), e.each(n, function (e, t) {
                        a[t] = p.css(t)
                    }), e(s).css(a), o(), window.chrome) {
                        var r = u.style.width;
                        u.style.width = "0px", u.offsetWidth, u.style.width = r
                    }
                }
                function r() {
                    var e, n;
                    t !== u ? a() : o(), s.value = u.value + i.append, s.style.overflowY = u.style.overflowY, n = parseInt(u.style.height, 10), s.scrollTop = 0, s.scrollTop = 9e4, e = s.scrollTop, d && e > d ? (u.style.overflowY = "scroll", e = d) : (u.style.overflowY = "hidden", c > e && (e = c)), e += w, n !== e && (u.style.height = e + "px", f && i.callback.call(u, u))
                }
                function l() {
                    clearTimeout(h), h = setTimeout(function () {
                        var e = p.width();
                        e !== g && (g = e, r())
                    }, parseInt(i.resizeDelay, 10))
                }
                var d, c, h, u = this, p = e(u), w = 0, f = e.isFunction(i.callback), z = {height: u.style.height, overflow: u.style.overflow, overflowY: u.style.overflowY, wordWrap: u.style.wordWrap, resize: u.style.resize}, g = p.width();
                p.data("autosize") || (p.data("autosize", !0), ("border-box" === p.css("box-sizing") || "border-box" === p.css("-moz-box-sizing") || "border-box" === p.css("-webkit-box-sizing")) && (w = p.outerHeight() - p.height()), c = Math.max(parseInt(p.css("minHeight"), 10) - w || 0, p.height()), p.css({overflow: "hidden", overflowY: "hidden", wordWrap: "break-word", resize: "none" === p.css("resize") || "vertical" === p.css("resize") ? "none" : "horizontal"}), "onpropertychange"in u ? "oninput"in u ? p.on("input.autosize keyup.autosize", r) : p.on("propertychange.autosize", function () {
                    "value" === event.propertyName && r()
                }) : p.on("input.autosize", r), i.resizeDelay !== !1 && e(window).on("resize.autosize", l), p.on("autosize.resize", r), p.on("autosize.resizeIncludeStyle", function () {
                    t = null, r()
                }), p.on("autosize.destroy", function () {
                    t = null, clearTimeout(h), e(window).off("resize", l), p.off("autosize").off(".autosize").css(z).removeData("autosize")
                }), r())
            })) : this
        }
    })(window.jQuery || window.$);

    var __slice = [].slice;
    (function (e, t) {
        var n;
        n = function () {
            function t(t, n) {
                var r, i, s, o = this;
                this.options = e.extend({}, this.defaults, n);
                this.$el = t;
                s = this.defaults;
                for (r in s) {
                    i = s[r];
                    if (this.$el.data(r) != null) {
                        this.options[r] = this.$el.data(r)
                    }
                }
                this.createStars();
                this.syncRating();
                this.$el.on("mouseover.starrr", "span", function (e) {
                    return o.syncRating(o.$el.find("span").index(e.currentTarget) + 1)
                });
                this.$el.on("mouseout.starrr", function () {
                    return o.syncRating()
                });
                this.$el.on("click.starrr", "span", function (e) {
                    return o.setRating(o.$el.find("span").index(e.currentTarget) + 1)
                });
                this.$el.on("starrr:change", this.options.change)
            }
            t.prototype.defaults = {rating: void 0, numStars: 5, change: function (e, t) {
                }};
            t.prototype.createStars = function () {
                var e, t, n;
                n = [];
                for (e = 1, t = this.options.numStars; 1 <= t ? e <= t : e >= t; 1 <= t ? e++ : e--) {
                    n.push(this.$el.append("<span class='glyphicon .glyphicon-star-empty'></span>"))
                }
                return n
            };
            t.prototype.setRating = function (e) {
                if (this.options.rating === e) {
                    e = void 0
                }
                this.options.rating = e;
                this.syncRating();
                return this.$el.trigger("starrr:change", e)
            };
            t.prototype.syncRating = function (e) {
                var t, n, r, i;
                e || (e = this.options.rating);
                if (e) {
                    for (t = n = 0, i = e - 1; 0 <= i ? n <= i : n >= i; t = 0 <= i ? ++n : --n) {
                        this.$el.find("span").eq(t).removeClass("glyphicon-star-empty").addClass("glyphicon-star")
                    }
                }
                if (e && e < 5) {
                    for (t = r = e; e <= 4 ? r <= 4 : r >= 4; t = e <= 4 ? ++r : --r) {
                        this.$el.find("span").eq(t).removeClass("glyphicon-star").addClass("glyphicon-star-empty")
                    }
                }
                if (!e) {
                    return this.$el.find("span").removeClass("glyphicon-star").addClass("glyphicon-star-empty")
                }
            };
            return t
        }();
        return e.fn.extend({starrr: function () {
                var t, r;
                r = arguments[0], t = 2 <= arguments.length ? __slice.call(arguments, 1) : [];
                return this.each(function () {
                    var i;
                    i = e(this).data("star-rating");
                    if (!i) {
                        e(this).data("star-rating", i = new n(e(this), r))
                    }
                    if (typeof r === "string") {
                        return i[r].apply(i, t)
                    }
                })
            }})
    })(window.jQuery, window);
    $(function () {
        return $(".starrr").starrr()
    })

    $(function () {

        $('#new-review').autosize({append: "\n"});

        var reviewBox = $('#post-review-box');
        var newReview = $('#new-review');
        var openReviewBtn = $('#open-review-box');
        var closeReviewBtn = $('#close-review-box');
        var ratingsField = $('#ratings-hidden');

        openReviewBtn.click(function (e)
        {
            reviewBox.slideDown(400, function ()
            {
                $('#new-review').trigger('autosize.resize');
                newReview.focus();
            });
            openReviewBtn.fadeOut(100);
            closeReviewBtn.show();
        });

        closeReviewBtn.click(function (e)
        {
            e.preventDefault();
            reviewBox.slideUp(300, function ()
            {
                newReview.focus();
                openReviewBtn.fadeIn(200);
            });
            closeReviewBtn.hide();

        });

        $('.starrr').on('starrr:change', function (e, value) {
            ratingsField.val(value);
        });
    });



    $(document).ready(function () {
        $("#search-box").keyup(function () {
            if ($(this).val() != '') {
                $.ajax({
                    type: "get",
                    url: "{{ route('select_fundraiser')}}",
                    data: 'keyword=' + $(this).val(),
                    //   dataType: 'json',
                    beforeSend: function () {
                        //  $("#search-box").css("background", "#FFF url(LoaderIcon.gif) no-repeat 165px");
                    },
                    success: function (data) {
                        $("#suggesstion-box").show();
                        $("#suggesstion-box").html(data);
                        $("#search-box").css("background", "#FFF");
                    }
                });
            } else {
                $("#suggesstion-box").hide();
                $(this).css("border", "1px solid red");

            }
        });
    });

//To select country name
    function selectFundraiser(val) {
        $("#search-box_box_fundraiser").val(val);
        $("#suggesstion-box").hide();
        $("#search-box").val($("#text_" + val).html());
    }



    $(document).ready(function () {
        $("#driver").click(function () {
            $("#text").slideToggle("slow");
        });

        $("#add_submit").click(function () {

            if ($('#add_fundraiser_name').val() != '') {
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
                $.ajax({
                    type: "POST",
                    url: "{{ route('add_fundraiser')}}",
                    data: 'keyword=' + $('#add_fundraiser_name').val(),
                    dataType: 'json',
                    beforeSend: function () {
//			$("#search-box").css("background","#FFF url(LoaderIcon.gif) no-repeat 165px");
                    },
                    success: function (json) {

                        if (json.success == 1) {
                            swal({
                                title: "Good job!",
                                text: "Fundraiser added successfully.",
                                icon: "success",
                            });
                            $("#search-box").val(json.last_id);
                            $("#search-box").val($('#add_fundraiser_name').val());
                            $("#text").slideToggle("fast");
                            $('#add_fundraiser_name').val('');
                        } else if (json.error == 1) {
                            swal({
                                title: "Oh noes!",
                                text: "This Fundraiser already in our database.",
                                icon: "error",
                            });
                            $('#add_fundraiser_name').css("border", "1px solid red");
                            ;
                        } else {
                            swal({
                                title: "Oh noes!",
                                text: "Some ting missing, Please Try Again.",
                                icon: "error",
                            });
                            $('#add_fundraiser_name').css("border", "1px solid red");
                            ;
                        }
                    }
                });
            } else {
                swal({
                    title: "Oh noes!",
                    text: "Please enter fundraiser name.",
                    icon: "error",
                });
                $('#add_fundraiser_name').css("border", "1px solid red");
                ;

            }
        });

    });

    @if (\Session::has('success'))

            swal({
                title: "Good Job",
                text: "Record Added Successfully.",
                icon: "success",
            });


    @endif

</script>