<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use App\tran_user;
use Session;
use Auth;
use Redirect;
use Validator;
use Illuminate\Http\Request;

class UsersController extends Controller {

    /**
     * Show the profile for the given user.
     *
     * @param  int  $id
     * @return View
     */
    public function insert(Request $request) {
        $rules = array(
            'name' => 'required', // make sure the email is an actual email
            'email' => 'required|email',
            'review' => 'required',
            'fundraiser_id' => 'required',
            'rating' => 'required'
        );

        $validator = Validator::make(Input::all(), $rules, [
                    'name.required' => 'Please enter the  name.',
                    'email.required' => 'Please enter the valid email address.',
                    'review.required' => 'Please enter the review.',
                    'fundraiser_id.required' => 'Please select fundraiser name.',
                    'rating.required' => 'Please select rating.',
        ]);

        if ($validator->fails()) {
            return Redirect::to('/')->withInput($request->all)->withErrors($validator);
        } else {
            $user = DB::table('rating_users')->where('email', '=', $request->email)->where('fundraiser_id', '=', $request->fundraiser_id)->first();

            if ($user === null) {
                // user doesn't exist
                $data = array(
                    'name' => $request->name,
                    'email' => $request->email,
                    'review' => $request->review,
                    'fundraiser_id' => $request->fundraiser_id,
                    'rating' => $request->rating,
                    'created_at' => date('Y-m-d h:i:s')
                );
                $data = DB::table('rating_users')->insert($data);
                Session::flash('success', 'Record Added Successfully.');
                return Redirect::to('/');
            } else {
                Session::flash('email_error', 'Review already submited by this Email Id for this fundraiser  .');
                return Redirect::to('/')->withInput($request->all);
            }
        }
    }

    public function index() {
        return view('rating');
    }

    public function add_fundraiser(Request $request) {

        $json = array();
        $json['success'] = $json['error'] = 0;
        $user = DB::table('fundraiser')->where('fundraiser_name', '=', $request->keyword)->first();
        if ($user === null) {
            $data = array(
                'fundraiser_name' => $request->keyword
            );
            $res = DB::table('fundraiser')->insertGetId($data);
            if ($res) {
                $json['success'] = 1;
                $json['last_id'] = $res;
            }
        } else {
            $json['error'] = 1;
        }

        echo json_encode($json);
    }

    public function select_fundraiser(Request $request) {

        $fundraiser = DB::table('fundraiser')->Where('fundraiser_name', 'like', '%' . $request->keyword . '%')->get()->toArray();
        $html = '';
        if (!empty($fundraiser)) {
            $html = '<ul id="country-list">';
            foreach ($fundraiser as $country) {
                $html .='<li onClick="selectFundraiser(' . $country->id . ');" id="text_' . $country->id . '">' . $country->fundraiser_name . '</li>';
            }
            $html .='</ul>';
        }

        echo $html;
    }

    public function list_fundraiser() {

        $result = $res =array();
        $fundraiser = DB::table('fundraiser')->get()->toArray();
        if (!empty($fundraiser)) {
            foreach ($fundraiser as $country) {
                $rating_users = DB::table('rating_users')->where('fundraiser_id', '=', $country->id)->get()->toArray();
                if ($rating_users) {
                    // dd($rating_users);
                    
                    $rate = 0;
                    foreach ($rating_users as $rating) {
                        
                        $rate += $rating->rating;
                    }
                    $res['id'] = $country->id;
                    $res['rating'] = number_format($rate / $count,2);
                    $res['name'] = $country->fundraiser_name;
                     $result[] = $res;
                }
               
            }
 
             if(!empty($result) && count($result) > 0)  {
            foreach ($result as $key => $row) {
                $price[$key] = $row['rating'];
            }
            array_multisort($price, SORT_DESC, $result);
             }
        }



        return view('rating_list')->with('result', $result);
    }

    public function rating_form(Request $request) {
        $fundraiser = array();
        if ($request->isMethod('get')) {
            $id = $request->route('client_id');
            $fundraiser = DB::table('fundraiser')->where('id', '=', $id)->first();
            //  dd($fundraiser);
            return view('rating')->with('result', $fundraiser);
        } else {
            return view('rating')->with('result', $fundraiser);
        }
    }

}
