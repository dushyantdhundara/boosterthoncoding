<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use App\tran_user;
use Session;
use Auth;
use Redirect;
use Validator;
use Illuminate\Http\Request;

class DashboardController extends Controller {

    /**
     * Show the profile for the given user.
     *
     * @param  int  $id
     * @return View
     */
    public function dashboard() {
        if (Session::get('data')['user_id']) {

            return view('index');
        } else {
            return Redirect::to('/admin/login');
        }
    }

    public function rating() {
        if (Session::get('data')['user_id']) {
            $user = DB::table('rating_users')
                    ->leftJoin('fundraiser','rating_users.fundraiser_id','fundraiser.id')
                    ->orderBy('rating_users.rating', 'desc')->get()->toArray();
            //dd($user); die;
             return view('user_rating')->with('result',$user);
        } else {
            return Redirect::to('/admin/login');
        }
    }

}
