<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use App\tran_user;
use Session;
use Auth;
use Redirect;
use Validator;
use Illuminate\Http\Request;

class AdminController extends Controller {

    /**
     * Show the profile for the given user.
     *
     * @param  int  $id
     * @return View
     */
   

    public function login(Request $request) {
       
        $rules = array(
            'email' => 'required|email',
            'password' => 'required',
        );

        $validator = Validator::make(Input::all(), $rules, [
                    'email.required' => 'Please Enter Name',
                    'password.required' => 'Please Enter Password',
        ]);

        if ($validator->fails()) {
            return Redirect::to('/admin/login')->withErrors($validator);
        } else {
            $user = DB::table('admin')->where('email', '=', $request->email)->where('password', '=', md5($request->password))->first();

            if ($user != null) {
               
                $the_session = array (
                    'user_id' => $user->id,
                    'login' => true,
                    'email' => $user->email
                    
                );
                Session::put('data', $the_session);
                 return Redirect::to('/user/rating');
            } else {
                Session::flash('email_error', 'Invalid Email And Password.');
                return Redirect::to('/admin/login');
            }
        }
    }

    public function login_form() {
        if (Session::get('data')['user_id']) {
          return Redirect::to('/admin/dashboard');
        }
        return view('login');
    }

    public function logout() {
         Session::flush();
         return Redirect::to('/admin/login');
    }
}
