<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/




Route::get('/', 'UsersController@rating_form')->name('/');

Route::get('/by/{client_id?}','UsersController@rating_form')->name('reportpanding');


Route::post('/users', 'UsersController@insert')->name('rating'); 

Route::get('/admin/login', 'Admin\AdminController@login_form'); 

Route::post('/admin/login', 'Admin\AdminController@login')->name('admin_login'); 

Route::get('/admin/dashboard', 'Admin\DashboardController@dashboard')->name('dashboard'); 
        
Route::get('/user/rating', 'Admin\DashboardController@rating')->name('userrating');   

Route::get('/admin/logout', 'Admin\AdminController@logout')->name('logout');     

Route::get('/users/select_fundraiser', 'UsersController@select_fundraiser')->name('select_fundraiser');  

Route::post('/users/add_fundraiser', 'UsersController@add_fundraiser')->name('add_fundraiser');  

Route::get('/list_rating', 'UsersController@list_fundraiser')->name('list_rating');  



//Route::get('/people','peopleController@index')->name('people');
//Route::get('/ttt', 'UsersController@insert')->name('rating'); 